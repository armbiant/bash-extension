# Bash Extension

A library of domain agnostic Bash functions, with clear names, which can be reused across projects as if they
were part of Bash itself.

Hopefully, this will make it simpler to create new Bash scripts, and it will make them more readable.

## How to use

Include the file `src/loader.sh` in your script, and you are good to go.

Ex:

```bash
#!/usr/bin/env bash

. ".../src/loader.sh"

if os.is_ubuntu; then
  echo "Is Ubuntu!"
elif os.is_alpine; then
  echo "Is Alpine!"
else
  echo "Unknown distribution"
fi
```

### Creating and running test suites

Create a test by naming it with the postfix `.unit_test.sh`, and put your tests in it while
naming the functions with the prefix `unit_test.`

The, run all your tests in that folder, with `.../bin/test.sh <tests_folder|'./tests'> <folder_for_temporary_tests_artifacts|'/tmp/tests'>`

Ex:
Create this test file and put it in a folder `./tests`:
```bash
#!/usr/bin/env bash

unit_test.it_should_test_something() {

  if [ "666" == "666" ]; then
    RESULT=0
  else
    RESULT=1
  fi

  return ${RESULT}
}
```

Execute this test, and all other tests in the folder in sub-folders, by running:
```shell
./vendor/hgraca/bash-extension/bin/test.sh ./tests ./var/tests
```

## Tests

Currently, there are no tests, but I hope to add some at some point.
