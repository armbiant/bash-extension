#!/usr/bin/env bash

# shellcheck source-path=SCRIPTDIR
DIR_bash_extension_bin=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
DIR_bash_extension_root="${DIR_bash_extension_bin}/.."
DIR_TESTS="${1:-'./tests'}"
DIR_TESTS_TMP="${2:-'/tmp/tests'}"
mkdir -p "${DIR_TESTS_TMP}"

. "${DIR_bash_extension_root}/src/loader.sh"

mapfile -t TEST_FILE_LIST < <(find "${DIR_TESTS}" -type f -name "*.unit_test.sh" -print)
for TEST_FILE in "${TEST_FILE_LIST[@]}"; do
  stdout.log_debug "Including test file '${TEST_FILE}'"
  # shellcheck disable=SC1090
  . "${TEST_FILE}"
  error.exit_if_error $?
done

TEST_LIST=()
mapfile -t TEST_LIST < <(compgen -A function | grep '^unit_test\.')
echo

echo
declare -A PID_LIST
for TEST in "${TEST_LIST[@]}"; do
  mkdir -p "${DIR_TESTS_TMP}/${TEST}"
  eval "${TEST}" > "${DIR_TESTS_TMP}/${TEST}/stdout.log" 2>&1 &
  PID_LIST[$!]="${TEST}"
done
parallel.wait_and_check_success "$(declare -p PID_LIST)"
RETURN_CODE=$?
echo
echo

if [ $RETURN_CODE == 0 ]; then
  stdout.log_success "All tests passed! Exit code: $RETURN_CODE"
else
  stdout.log_error "Tests failed! Exit code: $RETURN_CODE"
  for TEST in "${TEST_LIST[@]}"; do
    if [ -e "${DIR_TESTS_TMP}/${TEST}/stdout.log" ]; then
      stdout.log_debug "=================="
      stdout.log_debug "Output of ${TEST}:"
      cat "${DIR_TESTS_TMP}/${TEST}/stdout.log"
      stdout.log_debug "=================="
    fi
  done
fi
echo

exit $RETURN_CODE
