#!/usr/bin/env bash

# shellcheck source-path=SCRIPTDIR
DIR_bash_extension=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
. "${DIR_bash_extension}/functions/apt.sh"
. "${DIR_bash_extension}/functions/array.sh"
. "${DIR_bash_extension}/functions/dir.sh"
. "${DIR_bash_extension}/functions/error.sh"
. "${DIR_bash_extension}/functions/file.sh"
. "${DIR_bash_extension}/functions/github.sh"
. "${DIR_bash_extension}/functions/math.sh"
. "${DIR_bash_extension}/functions/os.sh"
. "${DIR_bash_extension}/functions/parallel.sh"
. "${DIR_bash_extension}/functions/runtime.sh"
. "${DIR_bash_extension}/functions/stdout.sh"
. "${DIR_bash_extension}/functions/string.sh"
. "${DIR_bash_extension}/functions/test.sh"
