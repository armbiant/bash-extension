#!/usr/bin/env bash

#
# Synopsis: update_apt_repos
#
REPO_COUNT=0
apt.update_apt_repos() {
  TMP="$(grep -hc ^deb /etc/apt/sources.list /etc/apt/sources.list.d/*)"
  if [[ ${TMP} -ne ${REPO_COUNT} ]]; then
    REPO_COUNT=${TMP}
    apt-get update
  fi
}

#
# Synopsis: update_apt_repo <repo_name>
#
apt.update_apt_repo() {
  apt-get update -o Dir::Etc::sourcelist="sources.list.d/${1}" -o Dir::Etc::sourceparts="-" -o APT::Get::List-Cleanup="0"
}

#
# Synopsis: add-apt-repo <name> <repo>
#
apt.add_apt_repo() {
  echo "${2}" >"/etc/apt/sources.list.d/${1}.list"
  update_apt_repo "${1}"
}

#
# Synopsis: add-apt-repo <repo>
#
apt.add_apt_ppa() {
  add-apt-repository "${1}"
  update_apt_repos
}
