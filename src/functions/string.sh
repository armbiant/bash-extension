#!/usr/bin/env bash

# Ex:
#  IN="something to search for"
#  NEEDLE="something to search=for"
#  EXTRACTED=$(string.extract_after_last IN NEEDLE) => "for"
string.extract_after_last() {
  declare STRING="$1"
  declare NEEDLE="$2"

  echo "${STRING#*${NEEDLE}}"
}

# Ex:
#   NAME='fahmida'
#   echo $(string.to_upper_first ${NAME} => Fahmida
string.to_upper_first() {
  declare STRING="$1"

  echo "${STRING^}"
}

# Ex:
#   NAME='fahmida'
#   echo $(string.to_upper ${NAME} => FAHMIDA
string.to_upper() {
  declare STRING="$1"

  echo "${STRING^^}"
}

#
# Replaces text in a file
# Synopsis: replaceText <filepath> <from> <to>
#
string.replaceTextInFile() {
  FILE_PATH="$1"
  FROM="$2"
  TO="$3"
  cp -f "${FILE_PATH}" "${FILE_PATH}.bkp"
  sed "s@${FROM}@${TO}@" "${FILE_PATH}.bkp" >"${FILE_PATH}"
}

#
# Replaces text in a string
# Synopsis: replaceText <haystack> <from> <to>
#
string.replaceText() {
  HAYSTACK="$1"
  FROM="$2"
  TO="$3"

  echo "${HAYSTACK//${FROM}/${TO}}"
}

string.leftTrim() {
  NEEDLE="$1"
  HAYSTACK="$2"
  echo "${HAYSTACK#"$NEEDLE"}"
}

string.rightTrim() {
  NEEDLE="$1"
  HAYSTACK="$2"
  echo "${HAYSTACK%"$NEEDLE"}"
}

string.contains() {
  NEEDLE="$1"
  HAYSTACK="$2"
  if [[ "$HAYSTACK" != *"$NEEDLE"* ]]; then
    return 1
  fi
}
