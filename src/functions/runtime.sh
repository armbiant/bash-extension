#!/usr/bin/env bash

DIR_error=$(cd -- "$(dirname -- "${BASH_SOURCE[0]}")" &>/dev/null && pwd)
. "${DIR_error}/array.sh"
. "${DIR_error}/stdout.sh"

runtime.is_valid_environment_or_exit() {
  VALID_ENVIRONMENTS=${VALID_ENVIRONMENTS:-('dev' 'test' 'staging' 'demo' 'prod')}
  ENV=${1}

  array.contains "${ENV}" "${VALID_ENVIRONMENTS[@]}"
  RETURN_CODE=$?
  if [ "${RETURN_CODE}" -ne "0" ]; then
    stdout.log_error "Error: Environment '${ENV}' not in list of valid environments: [${VALID_ENVIRONMENTS[*]}]"
    exit 3
  fi
}

runtime.function_exists() {
  FUNCTION=${1}

  if [ "$(type -t "${FUNCTION}")" != "function" ]; then
    return 1
  fi
}

#
# Ex:
#   PATH=$(runtime.script_dir_path "${BASH_SOURCE[0]}")
#
runtime.script_dir_path() {
  SCRIPT_PATH=${1}

  cd -- "$( dirname -- "${SCRIPT_PATH}" )" &> /dev/null && pwd
}
