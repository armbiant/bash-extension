#!/usr/bin/env bash

dir.count_dir() {
  PATH_TO_COUNT="$1"

  ls -da ${PATH_TO_COUNT}/*/ | wc -l
}
