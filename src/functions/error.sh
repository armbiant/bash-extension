#!/usr/bin/env bash

DIR_error=$(cd -- "$(dirname -- "${BASH_SOURCE[0]}")" &>/dev/null && pwd)
. "${DIR_error}/stdout.sh"

#
# Exits the script if the given code is different than 0
# Ex:
#   exit_if_error $SOME_EXIT_CODE
#
error.exit_if_error() {
  EXIT_CODE=${1:-'0'}

  if [ "${EXIT_CODE}" -ne "0" ]; then
    stdout.log_info "Exit code: ${EXIT_CODE}"
    exit "${EXIT_CODE}"
  fi

  return "0"
}

#
# Ex:
#   if is_error $SOME_EXIT_CODE; then
#     ...
#   fi
#
error.is_error() {
  CODE=${1:-'0'}

  if [ "${CODE}" -ne "0" ]; then
    stdout.log_error "Error code: ${CODE}"
    return 0 # returning 0 means "success", which in an IF statement evaluates to true
  fi

  return 1 # returning 1 means "failure", which in an IF statement evaluates to false
}
