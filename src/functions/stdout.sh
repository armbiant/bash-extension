#!/usr/bin/env bash

# TEXT COLORS (Bold High Intensity) @see https://dev.to/ifenna__/adding-colors-to-bash-scripts-48g4 https://www.codegrepper.com/code-examples/shell/bash+orange+color
TEXT_BLACK='\e[1;90m'
TEXT_RED='\e[1;91m'
TEXT_GREEN='\e[1;92m'
TEXT_YELLOW='\e[1;93m'
TEXT_BLUE='\e[1;94m'
TEXT_MAGENTA='\e[1;95m'
TEXT_CYAN='\e[1;96m'
TEXT_WHITE='\e[1;97m'
TEXT_CLEAR='\e[0m'

# Logging levels described by RFC 5424. https://github.com/Seldaek/monolog/blob/main/doc/01-usage.md#log-levels
BASH_EXT_LOG_LEVEL_EMERGENCY=600 # Emergency: system is unusable.
BASH_EXT_LOG_LEVEL_ALERT=550     # Action must be taken immediately. Example: Entire website down, database unavailable, etc. This should trigger the SMS alerts and wake you up.
BASH_EXT_LOG_LEVEL_CRITICAL=500  # Critical conditions. Example: Application component unavailable, unexpected exception.
BASH_EXT_LOG_LEVEL_ERROR=400     # Runtime errors that do not require immediate action but should typically be logged and monitored.
BASH_EXT_LOG_LEVEL_WARNING=300   # Exceptional occurrences that are not errors. Examples: Use of deprecated APIs, poor use of an API, undesirable things that are not necessarily wrong.
BASH_EXT_LOG_LEVEL_NOTICE=250    # Normal but significant events.
BASH_EXT_LOG_LEVEL_INFO=200      # Interesting events. Examples: User logs in, SQL logs.
BASH_EXT_LOG_LEVEL_DEBUG=100     # Detailed debug information.
BASH_EXT_LOG_LEVEL=${BASH_EXT_LOG_LEVEL:-${BASH_EXT_LOG_LEVEL_DEBUG}}

#
# function to log a msg
# ex: `log <error_level> <message>`
#
stdout.log() {

  ERROR_LEVEL="${1}"
  MESSAGE="${2}"

  case ${ERROR_LEVEL} in
    'emergency')
      COLOR=${TEXT_RED}
      ;;
    'alert')
      COLOR=${TEXT_RED}
      ;;
    'critical')
      COLOR=${TEXT_RED}
      ;;
    'error')
      COLOR=${TEXT_RED}
      ;;
    'warning')
      COLOR=${TEXT_MAGENTA}
      ;;
    'notice')
      COLOR=${TEXT_YELLOW}
      ;;
    'success')
      COLOR=${TEXT_GREEN}
      ;;
    'debug')
      COLOR=${TEXT_BLUE}
      ;;
    'info')
      COLOR=${TEXT_WHITE}
      ;;
  esac

  echo -e "${COLOR}[$(date '+%Y-%m-%d %H:%M:%S')][${ERROR_LEVEL^^}]: ${MESSAGE}${TEXT_CLEAR}"
}

stdout.log_emergency() {
  MESSAGE="${1}"

  if [ ${BASH_EXT_LOG_LEVEL} -gt ${BASH_EXT_LOG_LEVEL_EMERGENCY} ]; then
    return
  fi

  stdout.log 'emergency' "${MESSAGE}"
}

stdout.log_alert() {
  MESSAGE="${1}"

  if [ ${BASH_EXT_LOG_LEVEL} -gt ${BASH_EXT_LOG_LEVEL_ALERT} ]; then
    return
  fi

  stdout.log 'alert' "${MESSAGE}"
}

stdout.log_critical() {
  MESSAGE="${1}"

  if [ ${BASH_EXT_LOG_LEVEL} -gt ${BASH_EXT_LOG_LEVEL_CRITICAL} ]; then
    return
  fi

  stdout.log 'critical' "${MESSAGE}"
}

stdout.log_error() {
  MESSAGE="${1}"

  if [ ${BASH_EXT_LOG_LEVEL} -gt ${BASH_EXT_LOG_LEVEL_ERROR} ]; then
    return
  fi

  stdout.log 'error' "${MESSAGE}"
}

stdout.log_warning() {
  MESSAGE="${1}"

  if [ ${BASH_EXT_LOG_LEVEL} -gt ${BASH_EXT_LOG_LEVEL_WARNING} ]; then
    return
  fi

  stdout.log 'warning' "${MESSAGE}"
}

stdout.log_notice() {
  MESSAGE="${1}"

  if [ ${BASH_EXT_LOG_LEVEL} -gt ${BASH_EXT_LOG_LEVEL_NOTICE} ]; then
    return
  fi

  stdout.log 'notice' "${MESSAGE}"
}

stdout.log_info() {
  MESSAGE="${1}"

  if [ ${BASH_EXT_LOG_LEVEL} -gt ${BASH_EXT_LOG_LEVEL_INFO} ]; then
    return
  fi

  stdout.log 'info' "${MESSAGE}"
}

stdout.log_debug() {
  MESSAGE="${1}"

  if [ ${BASH_EXT_LOG_LEVEL} -gt ${BASH_EXT_LOG_LEVEL_DEBUG} ]; then
    return
  fi

  stdout.log 'debug' "${MESSAGE}"
}

stdout.log_success() {
  MESSAGE="${1}"

  stdout.log 'success' "${MESSAGE}"
}
