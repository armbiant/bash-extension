#!/usr/bin/env bash

#
# Replaces text in a file
# Synopsis: replaceText <filepath> <from> <to>
#
file.replace_text() {
  FILE_PATH="$1"
  FROM="$2"
  TO="$3"
  cp -f "${FILE_PATH}" "${FILE_PATH}.bkp"
  sed "s@${FROM}@${TO}@" "${FILE_PATH}.bkp" >"${FILE_PATH}"
}

file.exists() {
  FILE_PATH="$1"

  if [ -e "${FILE_PATH}" ] && [ ! -d "${FILE_PATH}" ]; then
    return 0
  fi

  return 1
}

file.have_same_content() {
  FILE_PATH_1="$1"
  FILE_PATH_2="$2"

  # option `-s` can't be `--silent` because older version of cmp doesn't support it
  cmp -s "${FILE_PATH_1}" "${FILE_PATH_2}"

  return $?
}
