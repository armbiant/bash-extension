#!/usr/bin/env bash

#
# Ex:
# isLinuxDistribution 'ubuntu'
# isLinuxDistribution 'alpine'
#
os.isLinuxDistribution() {
  cat '/etc/os-release' | grep "$1" >/dev/null
  return $?
}

os.is_alpine() {
  isLinuxDistribution 'alpine'
  return $?
}

os.is_ubuntu() {
  isLinuxDistribution 'ubuntu'
  return $?
}

#
# @return string ie: `x86_64`
#
os.getArch() {
  uname -m
}
