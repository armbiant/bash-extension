#!/usr/bin/env bash
# Bash 5.1

DIR_parallel=$(cd -- "$(dirname -- "${BASH_SOURCE[0]}")" &>/dev/null && pwd)
. "${DIR_parallel}/stdout.sh"

#
# Waits for a list of parallel threads to finish echoes its exit code and returns non-zero is any of them returns non-zero
# NOTE: It only works from Bash 5.1
# Ex:
#   declare -A PID_LIST
#   commandA 1 > a.out 2>&1 &
#   PID_LIST[$!]='Thread A'
#   commandB 2 > b.out 2>&1 &
#   PID_LIST[$!]='Thread B'
#   commandB 3 > c.out 2>&1 &
#   PID_LIST[$!]='Thread C'
#   parallel.wait_and_check_success "$(declare -p PID_LIST)"
#   echo "Exit code: $?"
#
parallel.wait_and_check_success() {
  eval "declare -A PID_LIST=${1#*=}" # eval string into a new associative array
  ERROR_COUNT=0
  while [ ${#PID_LIST[@]} -ne 0 ]; do
    wait -n -p PID "${!PID_LIST[@]}" # `-p` only from bash 5.1
    CODE=$?
    if [ "${CODE}" -ne "0" ]; then
      stdout.log_error "Task ${PID_LIST[$PID]} failed with exit code $CODE"
      ERROR_COUNT=$((ERROR_COUNT + 1))
    else
      stdout.log_success "Task ${PID_LIST[$PID]} successful"
    fi

    unset "PID_LIST[$PID]"
  done
  return "${ERROR_COUNT}"
}

parallel.cmd() {
  local PARALLELIZATION SCRIPT LOG_FILE="/tmp/parallel_cmd"
  local "${@}"

  # import all functions from the main script. To import only a specific function, use '$(typeset -f myfn)'
  local IMPORT_FUNCTIONS_DIRECTIVE
  IMPORT_FUNCTIONS_DIRECTIVE="$(typeset -f)"$'\n\n'

  declare -A PID_LIST=()

  for (( I = 1; I <= PARALLELIZATION; I++ )); do
    eval "${IMPORT_FUNCTIONS_DIRECTIVE}${SCRIPT}" > "${LOG_FILE}.$(date '+%Y%m%d%H%M%S').${I}.out" 2>&1 &
    PID_LIST[$!]=$(basename "${LOG_FILE}")
  done

  parallel.wait_and_check_success "$(declare -p PID_LIST)"

  return $?
}

parallel.ssh() {
  local SSH_KEY_FILE TARGET_LIST_STRING='' TARGET_LIST_FILE='' SCRIPT LOG_FILE="/tmp/parallel_ssh"
  local "${@}"

  if [ -e "${TARGET_LIST_FILE}" ]; then
    readarray -t TARGET_LIST < "${TARGET_LIST_FILE}"
  elif [ "${TARGET_LIST_STRING}" != '' ]; then
    # eval string into a new associative array
    eval "declare -A TARGET_LIST=${TARGET_LIST_STRING#*=}"
  else
    stdout.log_error "No SSH hostname list was given."
    exit 1
  fi

  # import all functions from the main script. To import only a specific function, use '$(typeset -f myfn)'
  local IMPORT_FUNCTIONS_DIRECTIVE
  IMPORT_FUNCTIONS_DIRECTIVE="$(typeset -f)"$'\n\n'

  declare -A PID_LIST=()
  for TARGET in "${TARGET_LIST[@]}"; do
    SANITIZED_TARGET=$(string.replaceText "${TARGET}" '/' '-')
    SANITIZED_TARGET=$(string.replaceText "${SANITIZED_TARGET}" ':' '-')
    FULL_LOG_FILE_PATH="./${LOG_FILE}.${SANITIZED_TARGET}.$(date '+%Y%m%d%H%M%S').out"
    if string.contains ":" "${TARGET}" || string.contains "@" "${TARGET}" || ( string.contains "." "${TARGET}" && ! string.contains "/" "${TARGET}" ) ; then
      stdout.log_debug "ssh -o \"StrictHostKeyChecking no\" -i \"${SSH_KEY_FILE}\" \"${TARGET}\" \"${SCRIPT}\" > \"${FULL_LOG_FILE_PATH}\" 2>&1 &"
      ssh -o "StrictHostKeyChecking no" -i "${SSH_KEY_FILE}" "${TARGET}" "${IMPORT_FUNCTIONS_DIRECTIVE}${SCRIPT}" > "${FULL_LOG_FILE_PATH}" 2>&1 &
    else
      stdout.log_debug "eval \"cd '${TARGET}' && ${SCRIPT}\" > \"${FULL_LOG_FILE_PATH}\" 2>&1 &"
      eval "cd '${TARGET}' && ${SCRIPT}" &
    fi
    PID_LIST[$!]=$(basename "${LOG_FILE}.${SANITIZED_TARGET}")
  done
  parallel.wait_and_check_success "$(declare -p PID_LIST)"

  return $?
}

parallel.rsync() {
  local SSH_KEY_FILE='' \
    SOURCE_PATH \
    DESTINATION_LIST_STRING='' \
    DESTINATION_LIST_FILE='' \
    DESTINATION_BASE_PATH='./' \
    LOG_FILE="/tmp/parallel_rsync" \
    OPTIONS="--recursive --links --times --update --delete --checksum" \
    EXCLUSION_PATHS="'.git' 'node_modules'"
  local "${@}"

  if [ -e "${DESTINATION_LIST_FILE}" ]; then
    readarray -t DESTINATION_LIST < "${DESTINATION_LIST_FILE}"
  elif [ "${DESTINATION_LIST_STRING}" != '' ]; then
    # eval string into a new associative array
    eval "declare -A DESTINATION_LIST=${DESTINATION_LIST_STRING#*=}"
  else
    stdout.log_error "No destination list was given."
    exit 1
  fi

  EXCLUSION_PATHS_LIST=(${EXCLUSION_PATHS})
  for EXCLUSION_PATH in "${EXCLUSION_PATHS_LIST[@]}"; do
    OPTIONS="${OPTIONS} --exclude=\"${EXCLUSION_PATH}\""
  done

  if [ "${SSH_KEY_FILE}" != '' ]; then
    OPTIONS="${OPTIONS} -e \"ssh -o 'StrictHostKeyChecking no' -i ${SSH_KEY_FILE}\""
  fi

  # import all functions from the main script. To import only a specific function, use '$(typeset -f myfn)'
  local IMPORT_FUNCTIONS_DIRECTIVE
  IMPORT_FUNCTIONS_DIRECTIVE="$(typeset -f)"$'\n\n'

  declare -A PID_LIST=()

  SOURCE_PATH="$(string.rightTrim '/' "${SOURCE_PATH}")/"
  for DESTINATION in "${DESTINATION_LIST[@]}"; do
    DESTINATION_BASENAME="$(basename "${DESTINATION}")"

    if [ "${SSH_KEY_FILE}" != '' ]; then
      if string.contains ':' "${DESTINATION}"; then
        COMPLETE_DESTINATION="$(string.rightTrim '/' "${DESTINATION}")/${DESTINATION_BASE_PATH}"
      else
        COMPLETE_DESTINATION="${DESTINATION}:${DESTINATION_BASE_PATH}"
      fi
    else
      COMPLETE_DESTINATION="$(string.rightTrim '/' "${DESTINATION}")/${DESTINATION_BASE_PATH}"
    fi

    # Need to use eval, otherwise the ${OPTIONS} are ignored
    stdout.log_debug "eval \"rsync \"${SOURCE_PATH}\" \"${COMPLETE_DESTINATION}\" ${OPTIONS} > \"${LOG_FILE}.${DESTINATION_BASENAME}.$(date '+%Y%m%d%H%M%S').out\" 2>&1\" &"
    eval "rsync \"${SOURCE_PATH}\" \"${COMPLETE_DESTINATION}\" ${OPTIONS} > \"${LOG_FILE}.${DESTINATION_BASENAME}.$(date '+%Y%m%d%H%M%S').out\" 2>&1" &
    PID_LIST[$!]=$(basename "${LOG_FILE}.${DESTINATION_BASENAME}")
  done

  parallel.wait_and_check_success "$(declare -p PID_LIST)"

  return $?
}
