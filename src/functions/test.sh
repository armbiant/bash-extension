#!/usr/bin/env bash

DIR_test=$(cd -- "$(dirname -- "${BASH_SOURCE[0]}")" &>/dev/null && pwd)
. "${DIR_test}/file.sh"
. "${DIR_test}/stdout.sh"

test.cleanup_fs() {
  TEST_PATH=${1}

  rm -rf "${TEST_PATH}"
}

test.assert_files_have_same_content() {
  FILE_PATH_1="$1"
  FILE_PATH_2="$2"

  if file.have_same_content "${FILE_PATH_1}" "${FILE_PATH_2}"; then
    return 0
  fi

  stdout.log_error "The files '${FILE_PATH_1}' and '${FILE_PATH_2}' don't have the same content."
  return 1
}

test.assert_file_exists() {
  FILE_PATH="$1"

  if file.exists "${FILE_PATH}"; then
    return 0
  fi

  stdout.log_error "The file '${FILE_PATH}' does not exist."
  return 1
}
